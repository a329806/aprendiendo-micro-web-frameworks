import tornado.ioloop
import tornado.web


class Prueba(tornado.web.RequestHandler):
    def get(self):
        self.write("La vandal mata mas bro")

    def post(self):
        usuario = self.get_argument("username")
        password = self.get_argument("password")
        self.write(f"Tu usuario es: {usuario} y password: {password}")

def make_app():
    return tornado.web.Application([
        (r"/", Prueba),
    ])

if __name__ == '__main__':
    app = make_app()
    app.listen(6942)
    tornado.ioloop.IOLoop.current().start()
